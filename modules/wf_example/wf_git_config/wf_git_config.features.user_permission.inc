<?php
/**
 * @file
 * wf_git_config.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function wf_git_config_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer git repositories'.
  $permissions['administer git repositories'] = array(
    'name' => 'administer git repositories',
    'roles' => array(
      'admin' => 'admin',
    ),
    'module' => 'git_repository',
  );

  // Exported permission: 'administer git servers'.
  $permissions['administer git servers'] = array(
    'name' => 'administer git servers',
    'roles' => array(
      'admin' => 'admin',
    ),
    'module' => 'git_server',
  );

  // Exported permission: 'administer worklow git'.
  $permissions['administer worklow git'] = array(
    'name' => 'administer worklow git',
    'roles' => array(
      'admin' => 'admin',
    ),
    'module' => 'wf_git',
  );

  return $permissions;
}
