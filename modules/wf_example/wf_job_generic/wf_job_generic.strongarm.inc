<?php
/**
 * @file
 * wf_job_generic.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function wf_job_generic_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_wf_job__job';
  $strongarm->value = array(
    'view_modes' => array(),
    'extra_fields' => array(
      'form' => array(
        'vid' => array(
          'weight' => '-49',
        ),
        'uuid' => array(
          'weight' => '-48',
        ),
        'title' => array(
          'weight' => '-45',
        ),
        'reference' => array(
          'weight' => '-44',
        ),
        'details' => array(
          'weight' => '-43',
        ),
        'jsid' => array(
          'weight' => '-42',
        ),
        'owner' => array(
          'weight' => '-41',
        ),
        'assigned' => array(
          'weight' => '-40',
        ),
        'eid' => array(
          'weight' => '-39',
        ),
        'created' => array(
          'weight' => '-38',
        ),
        'modified' => array(
          'weight' => '-37',
        ),
        'url' => array(
          'weight' => '-36',
        ),
        'edit_url' => array(
          'weight' => '-35',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_wf_job__job'] = $strongarm;

  return $export;
}
