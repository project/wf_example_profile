;
; WF Example Management System Make File.
;

core = 7
api = 2

;
; Core
;
projects[drupal] = "7.23"

;
; Contrib
;
projects[admin][version] = "2.0-beta3"
projects[admin][subdir] = "contrib"

projects[bean][version] = "1.2"
projects[bean][subdir] = "contrib"

projects[composer_manager][version] = "1.0-beta7"
projects[composer_manager][subdir] = "contrib"

projects[context][version] = "3.0-rc1"
projects[context][subdir] = "contrib"

projects[ctools][version] = "1.3"
projects[ctools][subdir] = "contrib"

projects[date][version] = "2.6"
projects[date][subdir] = "contrib"

projects[diff][version] = "3.2"
projects[diff][subdir] = "contrib"

projects[entity][version] = "1.2"
projects[entity][subdir] = "contrib"

projects[entitycache][version] = "1.1"
projects[entitycache][subdir] = "contrib"

projects[features][version] = "2.0-rc3"
projects[features][subdir] = "contrib"

projects[field_ipaddress][version] = "1.0-beta1"
projects[field_ipaddress][subdir] = "contrib"

projects[git][version] = "1.0-alpha2"
projects[git][subdir] = "contrib"

projects[jenkins][version] = "1.0-beta3"
projects[jenkins][subdir] = "contrib"

projects[memcache][version] = "1.0"
projects[memcache][subdir] = "contrib"

projects[module_filter][version] = "1.8"
projects[module_filter][subdir] = "contrib"

projects[pathauto][version] = "1.2"
projects[pathauto][subdir] = "contrib"

projects[realname][version] = "1.1"
projects[realname][subdir] = "contrib"

projects[rules][version] = "2.5"
projects[rules][subdir] = "contrib"

projects[services][version] = "3.5"
projects[services][subdir] = "contrib"

projects[shell_client][version] = "1.0-alpha1"
projects[shell_client][subdir] = "contrib"

projects[sshid][version] = "1.0-alpha1"
projects[sshid][subdir] = "contrib"

projects[sshkey][version] = "2.0"
projects[sshkey][subdir] = "contrib"

projects[strongarm][version] = "2.0"
projects[strongarm][patch][features][url] = "https://drupal.org/files/strongarm-2076543-import-export-value-alter-hooks.patch"
projects[strongarm][patch][features][md5] = "1c29a705964d5dbb03c33eb119a53cc7"
projects[strongarm][subdir] = "contrib"

projects[token][version] = "1.5"
projects[token][subdir] = "contrib"

;projects[uuid][version] = "1.0-alpha3"
projects[uuid][type] = "module"
projects[uuid][download][type] = "git"
projects[uuid][download][url] = "http://git.drupal.org/project/uuid.git"
projects[uuid][download][revision] = "22c3ae3f24e19ad411f14cb08b854895d4a6721b"
projects[uuid][subdir] = "contrib"

projects[views][version] = "3.7"
projects[views][subdir] = "contrib"

projects[wf] = "1.0-alpha2"

; Include dev tools
projects[meta_dev][subdir] = "dev"

;
; Themes
;
projects[eldir][type] = "theme"
projects[eldir][download][type] = "git"
projects[eldir][download][url] = "http://git.drupal.org/sandbox/skwashd/1907004.git"
projects[eldir][download][revision] = "be09c9eb994f4c08834d2e5ed178fd64dc131ca9"

;
; Libraries
;

