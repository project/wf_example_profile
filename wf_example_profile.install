<?php
/**
 * @file
 * Install, update and uninstall functions for the the wf_example_profile install profile.
 */

/**
 * Implements hook_install().
 *
 * Performs actions to set up the site for this profile.
 *
 * Try to keep this lightweight and handle as much config as possible in
 * features (or modules using CTools exportables).
 *
 * @see system_install()
 */
function wf_example_profile_install() {
  $theme = 'hefring';
  variable_set('theme_default', $hefring);
  // Enable the content in the content region, everything else will be handled by contexts exported to features.
  $default_theme = variable_get('theme_default', $theme);
  $values = array(
    array(
      'module' => 'system',
      'delta' => 'main',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'content',
      'pages' => '',
      'cache' => -1,
    ),
  );
  $query = db_insert('block')->fields(array('module', 'delta', 'theme', 'status', 'weight', 'region', 'pages', 'cache'));
  foreach ($values as $record) {
    $query->values($record);
  }
  $query->execute();

  // This is a closed environment so only permit administrators to create users.
  variable_set('user_register', USER_REGISTER_ADMINISTRATORS_ONLY);

  // Only allow authenticated users to view content.
  user_role_grant_permissions(DRUPAL_AUTHENTICATED_RID, array('access content'));

  $environments = array(
    'dev' => array(
      'env' => 'Dev',
      'drush_alias' => 'dev',
      'next_env_id' => NULL,
      'next_env' => 'stage',
      'notes' => t('Dev sandbox environments'),
      'status' => 1,
      'log' => 'Initial content',
    ),
    'stage' => array(
      'env' => 'Stage',
      'drush_alias' => 'stage',
      'next_env_id' => NULL,
      'next_env' => 'prod',
      'notes' => t('Stage environment'),
      'status' => 1,
      'log' => 'Initial content',
    ),
    'prod' => array(
      'env' => 'Prod',
      'drush_alias' => 'prod',
      'next_env_id' => NULL,
      'notes' => t('Production environment'),
      'status' => 1,
      'log' => 'Initial content',
    ),
  );

  $entities = array();
  foreach ($environments as $eid => $environment) {
    $environment['bundle'] = 'generic';
    $env = entity_get_controller('wf_environment')->create($environment);
    $result = wf_environment_save($env);
    $environments[$eid]['entity'] = $env;
  }

  foreach ($environments as $environment) {
    if (!empty($environment['next_env'])) {
      $env = $environment['entity'];
      $env->next_env_id = $environments[$environment['next_env']]['entity']->id;
      wf_environment_save($env);
    }
  }
}

